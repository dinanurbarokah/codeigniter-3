-- MariaDB dump 10.19  Distrib 10.4.24-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: dina
-- ------------------------------------------------------
-- Server version	10.4.24-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `User`
--

DROP TABLE IF EXISTS `User`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `User` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `level` enum('Admin','Member') NOT NULL,
  `role_id` int(255) NOT NULL,
  `is_active` int(1) NOT NULL,
  `image` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=113 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `User`
--

LOCK TABLES `User` WRITE;
/*!40000 ALTER TABLE `User` DISABLE KEYS */;
INSERT INTO `User` VALUES (100,'Dina Nur Barokah','16936@rpl.smkn1bawang.sch.id','dina','$2y$10$.ql9O6GrC8Uvtl6yYLIpuuGBSYusyCZHXSBjVIc/.P10wZdNbSJgW','Admin',1,1,'ivana-square.jpg'),(101,'Farras Arsya','putra@gmail.com','Putraaaaa','$2y$10$2y2Xttvex/YKZxFaf5x.cecfUIAWKqLPS3EKfkZUvAct8iaBnkYMO','Member',0,1,'team-2.jpg'),(111,'Winda Yustiani','winda@gmail.com','Winda','$2y$10$OMQj9DvbTj6vMbN/8Z0bwOZKslxyOViRYccw2ZH0pzi2LxU3Vq7bO','Admin',1,1,'marie.jpg'),(112,'Maulana Pratama','pratama@gmail.com','Tama','$2y$10$B0jF7v5Twnr3poihG0XUxOKRFKHdB0pE1WM3cik4FYWlc2Oys6ji.','Member',0,1,'team-3.jpg');
/*!40000 ALTER TABLE `User` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `daftar_obat`
--

DROP TABLE IF EXISTS `daftar_obat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `daftar_obat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(250) NOT NULL,
  `jenis` varchar(250) NOT NULL,
  `isi` varchar(255) NOT NULL,
  `harga_box` varchar(250) NOT NULL,
  `harga_satuan` varchar(250) NOT NULL,
  `stok_box` varchar(250) NOT NULL,
  `stok_biji` varchar(250) NOT NULL,
  `expired` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `daftar_obat`
--

LOCK TABLES `daftar_obat` WRITE;
/*!40000 ALTER TABLE `daftar_obat` DISABLE KEYS */;
INSERT INTO `daftar_obat` VALUES (2,'Bethadien','Cair','750 ml','-','Rp 8.000,00','','','15/08/2024'),(3,'Tolak angin','Cair','150 ml','Rp 25.000,00','Rp 2.000,00','','','25/01/2025'),(4,'Parasetamol','Cair','800 ml','-','Rp 11.500,00','','','31/03/2024'),(5,'OBH Combi','Cair','800 ml','-','Rp 7.500,00','','','29/11/2024'),(6,'Paramex','Tablet','10','Rp 32.500,00','Rp 1.000,00','','','20/03/2022'),(7,'Vicks Formula 44','Cair','500 ml','-','Rp 11.500,00','','','11/07/2022'),(8,'Albothyel','Cair','350 ml','-','Rp 7.700,00','','','30/10/2024'),(9,'Allopurinol','Tablet','12','Rp 43.500,00','Rp 2.000,00','','','21/06/2023'),(10,'Dermatix','Oles','25 gr','-','Rp 12.000,00','','','25/01/2025');
/*!40000 ALTER TABLE `daftar_obat` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `data_obat`
--

DROP TABLE IF EXISTS `data_obat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `data_obat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jenis` enum('Cair','Tablet','Kapsul','Oles','Tetes') NOT NULL,
  `harga` varchar(250) NOT NULL,
  `isi` varchar(250) NOT NULL,
  `expired` varchar(250) NOT NULL,
  `kemasan` varchar(250) NOT NULL,
  `stok` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `data_obat`
--

LOCK TABLES `data_obat` WRITE;
/*!40000 ALTER TABLE `data_obat` DISABLE KEYS */;
INSERT INTO `data_obat` VALUES (3,'Cair','Rp 26.500,00','750 ml','27/06/2023','Botol','55'),(6,'Oles','Rp 36.700,00','200 gr','11/12/2021','Plastik','13'),(16,'Tetes','Rp 19.500,00','500 ml','20/03/2022','Botol','96'),(19,'Tablet','Rp 4.000,00','10','30/09/2022','Plastik','14');
/*!40000 ALTER TABLE `data_obat` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kategori_obat`
--

DROP TABLE IF EXISTS `kategori_obat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kategori_obat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kategori` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kategori_obat`
--

LOCK TABLES `kategori_obat` WRITE;
/*!40000 ALTER TABLE `kategori_obat` DISABLE KEYS */;
INSERT INTO `kategori_obat` VALUES (1,'Obat Narkotika'),(2,'Obat Keras'),(3,'Obat Bebas'),(4,'Obat Terbatas');
/*!40000 ALTER TABLE `kategori_obat` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stok_obat`
--

DROP TABLE IF EXISTS `stok_obat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stok_obat` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `kode_obat` varchar(10) NOT NULL,
  `nama` varchar(250) NOT NULL,
  `jenis` varchar(250) NOT NULL,
  `kemasan` varchar(250) NOT NULL,
  `stok` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stok_obat`
--

LOCK TABLES `stok_obat` WRITE;
/*!40000 ALTER TABLE `stok_obat` DISABLE KEYS */;
INSERT INTO `stok_obat` VALUES (1,'JRD0001','Tolak angin','cair','tablet','29'),(2,'JRD0002','Promag','kapsul','tablet','50'),(3,'JRD0003','Parasetamol','Cair','Botol','11'),(10,'JRD0004','Parasetamol','Cair','Botol','60'),(11,'JRD0005','Salep 88','Oles','Botol','29');
/*!40000 ALTER TABLE `stok_obat` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-05-30 11:15:32
